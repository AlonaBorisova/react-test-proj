import React from 'react';
import PropTypes from 'prop-types';

export const TodoItem = (props) => {
  const handleToggle = () => props.handleToggle(props.id);
  const handleRemove = () => props.handleRemove(props.id, event);
  return(
      <li key={props.id} className="list-group-item">
        <div className="checkbox">
          <label>
            <input type="checkbox" onChange={handleToggle} checked={props.completed} value="" />
            <span>{props.title}</span>
          </label>
          <button onClick={handleRemove} type="button" className="btn btn-danger btn-xs">
            <span  className="glyphicon glyphicon-remove" />
          </button>
        </div>
      </li>
  )
};

TodoItem.propsType = {
  title: PropTypes.string.isRequired,
  completed: PropTypes.bool.isRequired,
  id: PropTypes.number.isRequired,
};