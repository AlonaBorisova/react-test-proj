import React from 'react';
import {Link} from '../router';

export const Footer = (props) => {
  return (
      <div className="footer nav nav-pills">
        <Link showTempMessage={props.showTempMessage} to="/">All</Link>
        <Link showTempMessage={props.showTempMessage} to="/active">Active</Link>
        <Link showTempMessage={props.showTempMessage} to="/complete">Complete</Link>
      </div>
  )
};