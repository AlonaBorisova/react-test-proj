import React from 'react';
import PropTypes from 'prop-types';
export const TodoForm = (props) => (
    <form onSubmit={props.handleSubmit}>
      <input type="text" className="form-control" placeholder="Text input and press Enter or just press Enter"
             onChange={props.handleInputChange}
             value={props.currentToDo}/>
    </form>);

TodoForm.propsType = {
  currentToDo: PropTypes.string,
  handleInputChange: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};
