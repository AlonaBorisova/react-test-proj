import React from 'react';
import PropTypes from 'prop-types';
import {TodoItem} from './todoItem';

export const TodoList = (props) => {
  return(
      <div className="todo-list">
        <ul className="list-group">
          {props.todos.map(todo => <TodoItem
              handleToggle={props.handleToggle}
              key={todo.id} {...todo}
              handleRemove={props.handleRemove}
          />)}
        </ul>
      </div>
  )
};

TodoList.propsType = {
  todos: PropTypes.array.isRequired,
};