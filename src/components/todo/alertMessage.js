import React from 'react';

export const AlertMessage = (props) => {
  return (
      <div className={props.class}>
        <span>{props.message}</span>
      </div>
  )
};

