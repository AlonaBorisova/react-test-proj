import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {AlertMessage} from '../todo';

export class Link extends Component {
  static contextTypes = {
    route: PropTypes.string,
    linkHandler: PropTypes.func
  };

  handleClick = (event) => {
    event.preventDefault();
    this.context.linkHandler(this.props.to);
    this.props.showTempMessage(`Route has changed to ${this.props.to} and there are left filtered todos`);
  };

  render() {
    const activeClass = this.context.route === this.props.to ? 'active' : '';
    return <li className={activeClass}><a href="#" className={activeClass} onClick={this.handleClick}>{this.props.children}</a></li>
  }
}

Link.propsType = {
  to: PropTypes.string.isRequired,
  showTempMessage: PropTypes.func
};