import React, { Component } from 'react';
import PropTypes from 'prop-types';
import logo from './logo.svg';
import './App.css';
import {TodoForm, TodoList, Footer, AlertMessage} from './components/todo';
import {addTodo, generateId, findById, toggleTodo,
  updateTodo, removeTodo, filterTodos} from './helpers/helpers';
import {loadTodos, createTodo, saveTodo, deleteTodo} from './services/fetchService';

class App extends Component {
  state = {
    todos: [],
    currentToDo: ''
  };

  handleInputChange = (event) => {
    this.setState({
      currentToDo: event.target.value
    })
  };

  handleSubmit = (event) => {
    event.preventDefault();
    const newTodo = {id: generateId(), title: this.state.currentToDo, completed: false}
    const updatedTodo = addTodo(this.state.todos, newTodo);

    this.setState({
      todos: updatedTodo,
      currentToDo: '',
      errMsg: ''
    });
    createTodo(newTodo).then((res) => this.showTempMessage(`Todo with id=${res.id} was added :-)`));
  };

  handleEmptySubmit = (event) =>{
    event.preventDefault();
    this.setState({
      errMsg: 'Input some text! Please.'
    })
  };

  handleToggle = (id) => {
    const todo = findById(id, this.state.todos);
    const toggled = toggleTodo(todo);
    const updatedTodos = updateTodo(this.state.todos, toggled);
    this.setState({todos: updatedTodos});
    // https://jsonplaceholder.typicode.com/  the resource will not be really updated on the server but it will be faked as if.
    if (id < 21) {
    saveTodo(toggled)
        .then((res) => this.showTempMessage(`Tode with id=${res.id} updated`));
    } else {
      this.showTempMessage(`Tode with id=${id} updated`);
    }
  };

  handleRemove = (id, event) => {
    event.preventDefault();
    const updatedTodos = removeTodo(this.state.todos, id);
    this.setState({todos: updatedTodos});
    // https://jsonplaceholder.typicode.com/  the resource will not be really deleted on the server but it will be faked as if.
    if (id < 21) {
      deleteTodo(id).then((res) => this.showTempMessage(`Todo deleted`));
    } else {
      this.showTempMessage(`Todo deleted`);
    }
  };

  showTempMessage = (msg) => {
    this.setState({message: msg})
    setTimeout(() => this.setState({message: ''}), 3500)
  };

  static contextTypes = {
    route: PropTypes.string
  };

  componentDidMount() {
    loadTodos().then(todos => this.setState({todos}));
  }

  render() {
    const submitHandler = this.state.currentToDo ? this.handleSubmit : this.handleEmptySubmit;
    const displayTodos = filterTodos(this.state.todos, this.context.route)
    return (
      <div className="app">
        <div className="app-header">
          <img src={logo} className="app-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>
        <div className="jumbotron col-sm-6 col-sm-offset-3">
          {this.state.errMsg && <AlertMessage class={'alert alert-danger'} message={this.state.errMsg} />}
          {this.state.message && <AlertMessage class={'alert alert-success'} message={this.state.message} />}
          <TodoForm
              handleInputChange={this.handleInputChange}
              currentToDo={this.state.currentToDo}
              handleSubmit={submitHandler}
          />
          <TodoList
              handleToggle={this.handleToggle}
              todos={displayTodos}
              handleRemove={this.handleRemove}
          />
          <Footer showTempMessage={this.showTempMessage} />
        </div>
      </div>
    );
  }
}

export default App;
